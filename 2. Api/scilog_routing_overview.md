# Scientific-Logistics Routing API Instructions

To create a new routing request, POST the following JSON to the API endpoint 'https://scilogoptimize.azurewebsites.netapi/RequestRoutingById'.  

```JSON
{  
   "Id":"my_unique_id",
   "CompletionCallbackUrl":"https://my.site.com/api/routingresult",
   "IsGZipCompressed":false,
}
```

Authentication is provided by adding the provided key in the 'x-functions-key' header in the request. The key must be kept secret as all requests processed for this key will be associated with your account.

If everything is valid, the API will respond with the following JSON payload:

```JSON
{  
   "Id": "my_unique_id",
   "StatusCode":0,
   "StatusMessage":"Routing Request Queued For Execution",
   "InputUrl":"https://routeoptimization.blob.core.windows.net/routing-input/request_by_id/636542295024215485?sv=2016-05-31&sr=b&sig=wp%2Fscw73qDKbjXmZDViMng7vJqgGTSovO%2FS2fNGV%2BXY%3D&se=2018-02-14T23%3A42%3A16Z&sp=w"
}
```

The InputUrl field contains a secure (and time limited) URL that is used to POST the JSON data from Scientific-Logistics Routing Engine Input Specification. The link is valid for 60 seconds. Microsoft provides Azure libraries for most commonly used languages. Here is an example of how to POST the data from a file on disk using C#:

```csharp
CloudBlockBlob inputCloudBlockBlob = new CloudBlockBlob(new Uri(InputUrl));
inputCloudBlockBlob.UploadFromFile(@"C:\path\to\file\input.json");
```

Data can also be posted from a database or other non-file based method.

Posting data will place a request for the optimization into a queue where it will be processed by the next available worker machine. Once the optimization has completed, the following message will be posted to the callback URL specified in the original HTTP request:

```json foo
{
  "Id": "my_unique_id",
  "StatusCode": 1,
  "StatusMessage": "Routing Completed Successfully",
  "OutputUrl": "https://routeoptimization.blob.core.windows.net/routing-output/request_by_id/636542307724809365?sv=2017-07-29&sr=b&sig=VMsdYv%2B8SKnn3gm3NT7E5NfNJIjDg2GzcuAUfM%2BBnHE%3D&se=2018-02-15T00%3A46%3A45Z&sp=r"
}
```

The OutputUrl can be used to retrieve the JSON output produced by the optimization engine. Here is an example of how to download the results to a string using C#:

```csharp
CloudBlockBlob outputCloudBlockBlob = new CloudBlockBlob(new Uri(OutputUrl));
string outputJson = await outputCloudBlockBlob.DownloadTextAsync();
```

The current list of status codes are as follows:

	Queued = 0
	Completed = 1
	FailedToParseInput = 2
	Unknown = 255
